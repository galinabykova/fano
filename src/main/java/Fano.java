import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class Fano {
    //избавиться от рекурсии
    //сортировка
    StringBuilder[] code;
    double[] ps;

    public Fano(double[] ps) {
        this.ps = ps;
        code = new StringBuilder[ps.length];
        for (int i = 0; i < code.length; ++i) {
            code[i] = new StringBuilder();
        }
        makeCode();
    }

    private void makeCode() {
        split(0, ps.length);
    }

    // возвращает индекс первого элемента второго множества
    private void split (int start, int end) {
        if (end - start <= 1) return;
        double one = ps[start];
        double two = 0;
        for (int i = start + 1; i < end; ++i) {
            two += ps[i];
        }
        double min = Math.abs(two - one);
        int mini = start + 1;
        for (int i = start + 2; i < end; ++i) {
            one += ps[i - 1];
            two -= ps[i - 1];
            double dif = Math.abs(two - one);
            if (dif < min) {
                min = dif;
                mini = i;
            }
        }
        for (int i = start; i < mini; ++i) {
            code[i].append('0');
        }
        for (int i = mini; i < end; ++i) {
            code[i].append('1');
        }
        split(start, mini);
        split(mini, end);
    }

}
