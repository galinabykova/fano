import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Less {
    public static void make(String fileName) {
        int[] arr = new int[256];
        int cnt = 0;
        try (DataInputStream fin = new DataInputStream(new FileInputStream(fileName))) {
            int i;
            while ((i = fin.read()) != -1) {
                arr[i]++;
                ++cnt;
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        try (DataInputStream fin = new DataInputStream(new FileInputStream(fileName));
             DataOutputStream fos=new DataOutputStream(new FileOutputStream(fileName + ".fano"))) {
            ArrayList<Pair> list = new ArrayList<>();
            for (int i = 0; i < 256; ++i) {
                list.add(new Pair(i, arr[i] * 1.0 / cnt));
                fos.writeInt(arr[i]);
            }
            list.sort(Pair::compareTo);
            double[] p = new double[256];
            int[] word = new int[256];
            for (int i = 0; i < 256; ++i) {
                word[i] = list.get(i).one;
                p[i] = list.get(i).two;
                //System.out.print(p[i] + " ");
            }
            Fano fano = new Fano(p);
            Map<Integer, String> map = new HashMap<>();
            for (int i = 0; i < 256; ++i) {
                map.put(word[i], fano.code[i].toString());
            }
         //   System.out.println(map);
            ArrayList<Boolean> bools = new ArrayList<Boolean>();
            int i;
            while ((i = fin.read()) != -1) {
                //System.out.print(i + " ");
                for (char j : map.get(i).toCharArray()) {
                    if (j == '0') {
                        bools.add(false);
                     //   System.out.print("0");
                    } else {
                        bools.add(true);
                      //  System.out.print("1");
                    }
                }
            }
            cnt = 0;
            int now = 0;
            for (i = 0; i < bools.size(); ++i) {
                now = (now << 1);
                if (bools.get(i)) {
                    ++now;
                } else {
                }
                ++cnt;
                if (cnt >= 8) {
                    fos.write(now);
                    now = cnt = 0;
                }
            }
            if (cnt > 0) {
                while (cnt < 8) {
                    now *= 2;
                    ++cnt;
                }
                fos.write(now);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
