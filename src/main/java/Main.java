public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
           System.out.println("укажите имя файла");
           return;
        }
        if (args[0].endsWith(".fano")) {
            More.make(args[0].substring(0, args[0].length() - 5));
        } else {
            Less.make(args[0]);
        }
    }
}
