import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class More {
    public static void make(String fileName) {
        int[] arr = new int[256];
        int cnt = 0;
        try (DataInputStream fin = new DataInputStream(new FileInputStream(fileName + ".fano"));
             DataOutputStream fos=new DataOutputStream(new FileOutputStream(fileName))) {
            for (int i = 0; i < 256; ++i) {
                arr[i] = fin.readInt();
                cnt += arr[i];
            }
           // System.out.println(cnt);
            ArrayList<Pair> list = new ArrayList<>();
            for (int i = 0; i < 256; ++i) {
                list.add(new Pair(i, arr[i] * 1.0 / cnt));
            }
            list.sort(Pair::compareTo);
            double[] p = new double[256];
            int[] word = new int[256];
            for (int i = 0; i < 256; ++i) {
                word[i] = list.get(i).one;
                p[i] = list.get(i).two;
            }
            Fano fano = new Fano(p);
            Map<String, Integer> map = new HashMap<>();
            for (int i = 0; i < 256; ++i) {
                map.put(fano.code[i].toString(), word[i]);
            }
            int i;
            cnt = 0;
            StringBuilder a = new StringBuilder();
            while ((i = fin.read()) != -1) {
                i %= 256;
                while (cnt < 8) {
                    int now = i / 128;
                    if (now == 0) {
                        a.append("0");
                    } else {
                        a.append("1");
                    }
                    if (map.containsKey(a.toString())) {
                        fos.write(map.get(a.toString()));
                        a = new StringBuilder();
                    }
                    i = i << 1;
                    i %= 256;
                    ++cnt;
                }
                cnt = 0;
            }

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
