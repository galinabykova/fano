public class Pair implements Comparable <Pair>{
    Integer one;
    Double two;

    public Pair(Integer one, Double two) {
        this.one = one;
        this.two = two;
    }

    @Override
    public int compareTo(Pair o) {
        return o.two.compareTo(two);
    }

    @Override
    public String toString() {
        return one + " " + two;
    }
}
